
A Module that blacklisted configured emails ids as blacklisted from 
all outgoing website emails.


Installation
------------

Copy email blacklist module to your module directory and then enable 
on the admin modules page.  Configure the modules under 
admin/config/system/email_blacklist list out users email ids to 
whom you want to blacklisted for all outgoing website emails.

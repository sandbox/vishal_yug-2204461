<?php
/**
 * @file
 * Admin page callback file for the email_blacklist module.
 */

/**
 * Admin configuration form.
 */
function email_blacklist_admin_config_form() {
  $form = array();

  $mails = db_select('email_blacklist', 'b')
        ->fields('b', array('mail'))
        ->execute()
        ->fetchCol();

  $form['email_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Email Ids'),
    '#description' => t('Provide email ids of those users for which you want to blacklist. Enter each email id on a new line.'),
    '#default_value' => implode("\r", $mails),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Validation callback for admin configuration form.
 */
function email_blacklist_admin_config_form_validate($form, &$form_state) {
  $email_ids_area = check_plain($form_state['values']['email_ids']);
  $data = str_replace(array("\r", "\n"), ',', $email_ids_area);
  $email_ids = array_filter(explode(',', $data));

  $new_email_ids = array();

  foreach ($email_ids as $mail) {
    $error = user_validate_mail($mail);
    
	if ($error) {
		form_set_error('email_ids',$error);
	}
	
	if (!empty($mail)) {
      $new_email_ids[] = $mail;
    }
  }

  $form_state['values']['email_ids'] = $new_email_ids;
}

/**
 * Submit callback for admin configuration form.
 */
function email_blacklist_admin_config_form_submit($form, &$form_state) {
  $email_ids = $form_state['values']['email_ids'];
  $trimmed_email_ids = array();

  foreach ($email_ids as $mail) {
    $mail = trim($mail);

    db_merge('email_blacklist')->key(array('mail' => $mail))->execute();

    $trimmed_email_ids[] = $mail;
  }

  // Now delete all the keys that are not in email_ids.
  db_delete('email_blacklist')->condition('mail', $trimmed_email_ids, 'NOT IN')->execute();

  drupal_set_message(t('Your changes have been saved.'));
}
